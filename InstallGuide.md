# Guía de instalación para las pruebas de Spark en Java

## Obtención de los instaladores, ejecutables y scripts

Para realizar las pruebas necesitarás descargarte las siguientes herramientas:

  * Un instalador de Java8 [`jdk-8u281-windows-x64.exe`](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html#license-lightbox)
  * Un instalador del IDE IntelliJ-IDEA [`ideaIC-2020.3.2.exe`](https://download.jetbrains.com/idea/ideaIC-2020.3.2.exe)
  * Un archivo con las librerias de Spark2.4.7 [`spark-2.4.7-bin-hadoop2.6.tgz`](https://www.apache.org/dyn/closer.lua/spark/spark-2.4.7/spark-2.4.7-bin-hadoop2.6.tgz)
  * Un ejecutable que permite usar hadoop en el entorno Windows [`winutils.exe`](https://github.com/steveloughran/winutils/blob/master/hadoop-2.6.4/bin/winutils.exe)
  * Un script de PowerShell que hace el setup de las variables de entorno [`set_env_vars.ps1`](https://bitbucket.org/rpt1g12/spark_college/downloads/set_env_vars.ps1)
  
## Pasos de instalación

A continuación se detallan los pasos de instalación de las herramientas necesarias.

### 1. JAVA

Ejecuta el instalador `jdk-8u281-windows-x64.exe` pero asegurate que la instalación se
realiza en

```commandline
    C:\Program Files\Java\jdk1.8.0_281
```

### 2. IDE-IntelliJ-IDEA

Simplemente ejecuta el instalador `ideaIC-2020.3.2.exe`

### 3. SPARK

Para la instalación de Spark se deberán seguir los siguientes pasos:

    1. Crear una carpeta en el directorio raiz con el nombre *hadoop* y dentro de ella otra
    con el nombre *bin*
    
```commandline
    C:\hadoop\bin
```
    2. Copiar el ejecutable `winutils.exe` en la capeta *bin* creada en el paso anterior.
    3. Crear una carpeta en el directorio raiz con el nombre *spark*
    
```commandline
    C:\spark
```
    4. Descomprimir el archivo `spark-2.4.7-bin-hadoop2.6.tgz` en la capeta *spark* creada en el paso anterior.
    
```commandline
    C:\spark\spark-2.4.7-bin-hadoop2.6
```

    5. Ejecutar el script `set_env_vars.ps1`. Si has decidido cambiar algún path de instalación en los 
    pasos anteriores, deberás adaptar el script con los cambios pertinentes.
    Para comprobar que el script se ejecutó correctamente revisa tus variables de entorno de usuario utilizando
    las herramientas proporcionadas por Windows.
    
### 4. Comprobación

Si todo se ha instalado correctamente deberias poder:

    1. Abrir una consola de `Powershell` o linea de comandos `CMD` y ejecutar:

```commandline
    spark-shell
```
    
## Setup IntelliJ

### 1. Plugin de Scala

Instala el plugin de Scala para permitir su soporte disponible entre los plugins oficiales 
de IntelliJ.

### 2. Creación del Proyecto Maven
Una vez tengas todo instalado, deberás crear un proyecto `Maven` en `IntelliJ`. 

Te recomendamos que utilices la opción de crear un proyecto a partir de `GIT`. Para ello utiliza el repositorio
[spark_college](https://rpt1g12@bitbucket.org/rpt1g12/spark_college.git). Asegúrate de que el proyecto se importa
como un proyecto de tipo `Maven`.

### 3. Añadir librerias de Spark al proyecto.

    1. Abre los settings del proyecto (`File->Project Structure`)
    2. En el apratado `SDKs` comprueba que dispones de la JDK que instalaste previamente. Si no fuera así,
    dándole al símbolo `+` puedes añadirla seleccionando la carpeta donde hayas instalado Java.
    3. En el apartado `Global Libraries` pulsa el símbolo `+` y añade los JARS de Spark seleccionando la 
    carpeta

```commandline
    C:\spark\spark-2.4.7-bin-hadoop2.6\jars
```

    4. En el apartado `Global Libraries` pulsa el símbolo `+` y añade una Scala-SDK de la versión 2.11.8
    5. Una vez añadidas las librerías de los dos últimos pasos deberás añadirlas al módulo `Sparker` haciendo
    click derecho en cada una de ellas y `Add to modules`.
    
### 4. Maven

    1. Abre los settings de Maven (`File->Settings->Build, Execution, Deployement->Build Tools->Maven`)
    y selecciona la opción `Use plugin registry`
