# Prueba de Spark con Java

En esta prueba se evaluará tu conocimiento tanto de Java como de Spark.

En primer lugar sigue las [instrucciones de instalación](InstallGuide.md)

## 1. API DATAFRAMES

En este apartado se valorarán tus conocimientos de Spark y manejo de tablas.

Se te proporcionan dos tablas:

### LOGS

Logs de peticiones html que puedes encontrar [aquí](Sparker/src/main/resources/logs.json)

#### Descripción de la información: 

    - `clientIp`: Ip del cliente que realiza la petición al servicio web
    - `http_code`: código de respuesta de la petición html
    - `navigator`: Traza de `User-Agent` de las cabeceras de las peticiones html. 
	Para más información consultar esta documentación 
	[User-Agent](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/User-Agent)
    - `realDate`: Fecha en la que se lanza la solicitud
    - `refererId`: Identificador de la página de la que proviene la solicitud html 

### CODCOT

Tabla con información de cotizaciones que puedes encontrar [aquí](Sparker/src/main/resources/codcot.json).

#### Descripción de la información: 

    * `IPADDRESS`: Ip del cliente que realiza la petición al servicio web
    * `SESION_ID`: Sesión generada por el sistema de cotización 
    * `appName`: Aplicación web a la que se esta llamando
    * `codCot`: código de cotización en caso de que existiera 
    * `codInductor`: código de induccion de agregadores (rastreator)
    * `cookieGA`: identificador de google analytics
    * `idEstado`: identificador de pagina
    * `idSession`: sesión del navegador del cliente 
    * `userAgent`: Traza de `User-Agent` de las cabeceras de las peticiones html. 
    Para más información consultar esta documentación 
    [User-Agent](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/User-Agent) 
    * `realDate`: Fecha en la que se accede a la página de la aplicación
    
### Ejercicios:

Para resolver los ejercicios deberás implementar la interfaz `org.example.solvers.SolverDataframes`
de tal modo que la clase [MainDataframes](Sparker/src/main/java/org/example/MainDataframes.java)
compile y se ejecute.

#### Ejercicio 1

Leer los JSONS [LOGS](Sparker/src/main/resources/logs.json) y 
[CodCot](Sparker/src/main/resources/codcot.json) en sendos `Dataframes` de Spark.

#### Ejercicio 2

Seleccionar la primera navegación con cotización informada de la tabla `CODCOT` con el fin de 
saber a que hora entró un usuario por primera vez en el proceso de cotización. 
Es importante poder separar cada petición, no mezclar varias cotizaciones, y ordenarlas para saber 
cuando comienza. 

La solución sería el primer registro de entrada en el proceso de cotización. 

>*Notas*: 
>Hay que buscar un identificador lo mas unívoco para el proceso de cotización

#### Ejercicio 3 

Del resultado anterior anexar el `refererId` de la página de la que proviene. 
Para esto hay que buscar una clave que lo más unívoca posible con el fin de poder realizar un proceso de 
paralelización del problema eficiente. 

>*Notas*:
>   1 Los sistemas no tienen porque estar coordinados en tiempo por lo que hay que buscar un criterio de aproximación;
>   2 Entre los sistemas hay que unir lo mejor posible ambos origenes de datos;
>   3 Supuestamente el registro dentro de `LOGS` siempre ocurre antes que lo que ocurre en `CodCot`.

#### Ejercicio 4

Del problema anterior ver que registros de `LOGS` no han podido ser pareados con los registros 
del resultado del problema 1

## 2. API DATASETS

En este apartado se valorarán tus conocimientos de programación orientada a objetos mediante
el uso de la API de Datasets de Spark.

Para resolver los ejercicios deberás implementar la interfaz `org.example.solvers.SolverDatasets`
así como modificar las clases `org.example.data.Sample` y `org.example.data.DomainTypeCount`
de tal modo que la clase [MainDatasets](Sparker/src/main/java/org/example/MainDatasets.java)
compile y se ejecute.


### Ejercicios:

Puedes encontrar más documentación de como implementar las clases en los comentarios y las
descripciones `JavaDoc` de éstas.

#### Ejercicio 1:
Leer el archivo `src/main/resources/url_dataset.csv` en una lista de `String`

#### Ejercicio 2:
Paralelizar la lista obtenida en el ejercicio anterior en un Dataset de objetos `String`

#### Ejercicio 3:
Convertir el Dataset anterior en un Dataset de objetos `Sample`.

El objeto `Sample` debe tener como atributos:

    * Un `String` que indique el Nombre del Dominio de la URL (`domainName`).
    * Un `String` que indique el tipo de la URL del dominio (`domainType`)
        Por tipo de URL nos referimos a la terminación de la URL. Es decir,
            * Para "www.google.com" su tipo sería "com"
            * Para "www.brexit.gov.uk" su tipo seria "uk"

#### Ejercicio 4:
Hacer un conteo de tipos de URL sin utilzar funciones de la API de DataFrames de Spark. Es decir,
no puedes utilizar la función `.groupBy`, ya que esta convierte el resultado en un DataFrame en 
lugar de un Dataset.

Para ello, deberás presentar los resultados en un Dataset de objetos `DomainTypeCount` cuyos
atributos son:

    * Un `String` que indique el tipo de la URL del dominio (`domainType`)
    * Un `long` que indique cuántos casos de ese tipo existen.
