package org.example;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.example.solvers.SolverDataframes;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;


public class MainDataframes {
    // Path a los datos de entrada.
    final static Path LOGS_PATH = Paths.get("src/main/resources/logs.json");
    final static Path CODCOT_PATH = Paths.get("src/main/resources/codcot.json");

    // Sesion de spark.
    final SparkSession sparkSession;
    // Objeto solver a utilizar para resolver los ejercicios.
    final SolverDataframes solver;

    /**
     * Constructor
     */
    public MainDataframes(SolverDataframes solver){
        this.sparkSession = SparkSession.builder().master("local[*]").getOrCreate();
        this.solver = solver;
    }

    public static void main(String[] args){
        // Aquí debes proporcionar tu implementación de la interfaz SolverDatasets
        MainDataframes main = new MainDataframes(new MySolver());
        try {
            main.run();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void run() throws FileNotFoundException {

        /*
            Ejercicio 1: Leer los JSONS
         */

        Dataset<Row> logs = solver.readJson(LOGS_PATH);
        Dataset<Row> codCot = solver.readJson(CODCOT_PATH);
        System.out.println(String.format("#logs: %s",logs.count()));
        System.out.println(String.format("#codCot: %s",codCot.count()));

        /*
            Ejercicio 2: Encontrar los primeros registros con cotizacion informada.
         */

        Dataset<Row> firstEntryDs = solver.firstEntry(codCot);
        System.out.println(String.format("#firstEntryDs: %s",firstEntryDs.count()));

        /*
            Ejercicio 3: Anexar la columna `refererId`
         */

        Dataset<Row> withRefererId = solver.appendRefererId(firstEntryDs,logs);
        System.out.println(String.format("#withRefererId: %s",withRefererId.count()));

        /*
            Ejercicio 4: Encontrar los registros no pareados de logs
         */

        Dataset<Row> notMatched = solver.findNotMatched(withRefererId, logs);
        System.out.println(String.format("#notMatched: %s",notMatched.count()));
    }
}
