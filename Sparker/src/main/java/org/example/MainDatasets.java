package org.example;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.example.data.DomainTypeCount;
import org.example.data.Sample;
import org.example.solvers.SolverDatasets;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class MainDatasets {
    // Path a los datos de entrada.
    final static Path DATA_PATH = Paths.get("src/main/resources/url_dataset.csv");

    // Objeto solver a utilizar para resolver los ejercicios.
    final SolverDatasets solver;
    // Sesion de spark.
    final SparkSession sparkSession;

    /**
     * Constructor
     * @param solver Tu implementacion de la interfaz {@link SolverDatasets}
     */
    public MainDatasets(SolverDatasets solver){
        this.solver = solver;
        this.sparkSession = SparkSession.builder().master("local[*]").getOrCreate();
    }

    public static void main(String[] args){
        // Aquí debes proporcionar tu implementación de la interfaz SolverDatasets
        MainDatasets main = new MainDatasets(new MySolver());
        try {
            main.run();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void run() throws FileNotFoundException {
        /*
            Ejercicio 1:
                + Leer el archivo en una lista de strings en la que cada string representa una linea.
         */
        List<String> lines = solver.readLines(DATA_PATH);

        /*
            Ejercicio 2:
                + Convertir la lista de strings a un Dataset de Strings.
         */
        Dataset<String> strings = solver.parallelizeStringList(lines);
        System.out.println(String.format("#strings: %s", strings.count()));

        /*
            Ejercicio 3:
                + Convertir el Dataset de Strings en otro de objetos Sample.
         */
        Dataset<Sample> samples = solver.toSampleDs(strings).cache();
        System.out.println(String.format("#samples: %s",samples.count()));

        /*
            Ejercicio 4:
                + Aggrupar el dataset anterior en objetos DomainTypeCount que resumen la cantidad
                  de objetos Sample con el mismo valor del atributo DomainType.
         */
        Dataset<DomainTypeCount> domainTypeCounts = solver.getDomainTypeCounts(samples).cache();
        System.out.println(String.format("#domainTypeCounts: %s",domainTypeCounts.count()));
        domainTypeCounts.show(false);
    }
}
