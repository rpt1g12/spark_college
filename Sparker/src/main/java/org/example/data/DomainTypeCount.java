package org.example.data;

/**
 * Objeto que describe la cantidad existente de {@link Sample}s con un tipo de
 * dominio.
 */
public class DomainTypeCount {
    String domainType;
    long count;

    /**
     * Constructor de la clase.
     *
     * @param domainType Tipo de dominio.
     * @param count      Conteo.
     */
    public DomainTypeCount(String domainType, long count) {
        this.domainType = domainType;
        this.count = count;
    }
}
