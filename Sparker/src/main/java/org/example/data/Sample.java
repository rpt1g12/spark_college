package org.example.data;

/**
 * Clase que representa una muestra del dataset de URLs.
 */
public class Sample {
    private String domainName;
    private String domainType;
    private boolean good;

    /**
     * Constructor de la clase.
     *
     * @param domainName Nombre del dominio.
     * @param domainType Tipo del dominio. Por ejemplo, en {@code "http://wwww.google.com"}
     *                   seria {@link "com"}
     * @param good       Indicador de si la URL es maliciosa o no.
     */
    public Sample(String domainName, String domainType, boolean good) {
        this.domainName = domainName;
        this.domainType = domainType;
        this.good = good;
    }
}
