package org.example.solvers;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.nio.file.Path;

public interface SolverDataframes extends Serializable {

    /**
     * Devuelve una sesion de spark que debe haber sido iniciada con anterioridad.
     *
     * @return Sesion de Spark
     */
    default SparkSession getSparkSession() {
        return SparkSession.builder().getOrCreate();
    }

    /**
     * Lee un fichero JSON en un Dataframe de Spark.
     *
     * @param path Path al fichero JSON.
     * @return Dataframe con los datos del JSON.
     * @throws FileNotFoundException Si no se encuentra el archivo JSON.
     */
    Dataset<Row> readJson(Path path) throws FileNotFoundException;

    /**
     * Devuelve un Dataframe con los registros de primerera entrada en el proceso de cotizacion.
     *
     * @param inputCodCot Dataframe CodCot.
     * @return Dataframe con los registros de primerera entrada en el proceso de cotizacion.
     */
    Dataset<Row> firstEntry(Dataset<Row> inputCodCot);

    /**
     * Anexa la columna `refererId` del Dataframe `logs` al Dataframe `firstEntryDs`.
     * @param firstEntryDs Dataframe resultante de la funcion {@link #firstEntry(Dataset)}
     * @param logs Dataframe Logs
     * @return Dataframe Logs con la columna `refererId` anexada.
     */
    Dataset<Row> appendRefererId(Dataset<Row> firstEntryDs, Dataset<Row> logs);

    /**
     * Devuelve los registros de `LOGS` que no han podido ser pareados con los registros
     * resultantes de {@link #appendRefererId(Dataset, Dataset)}
     * @param withRefererId Dataframe resultante de {@link #appendRefererId(Dataset, Dataset)}
     * @param logs Dataframe Logs
     * @return Dataframe con los registros de `LOGS` que no han podido ser pareados.
     */
    Dataset<Row> findNotMatched(Dataset<Row> withRefererId, Dataset<Row> logs);
}
