package org.example.solvers;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.MapGroupsFunction;
import org.apache.spark.api.java.function.MapPartitionsFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.SparkSession;
import org.example.data.DomainTypeCount;
import org.example.data.Sample;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.List;

public interface SolverDatasets extends Serializable {

    /**
     * Devuelve una sesion de spark que debe haber sido iniciada con anterioridad.
     *
     * @return Sesion de Spark
     */
    default SparkSession getSparkSession() {
        return SparkSession.builder().getOrCreate();
    }

    /**
     * Lee un fichero de texto y lo convierte en una lista de objetos String en la
     * que cada objeto representa una linea del fichero indicado.
     *
     * @param path Path al fichero de texto.
     * @return Una lista de strings en la que cada una representa una linea del fichero
     * indicado.
     * @throws FileNotFoundException Si no se encuentra el archivo de texto.
     */
    List<String> readLines(Path path) throws FileNotFoundException;

    /**
     * Paraleliza una lista de strings en un Dataset de Strings
     *
     * @param stings Lista de strings
     * @return Dataset de strings
     */
    Dataset<String> parallelizeStringList(List<String> stings);

    /**
     * Convierte un objeto String en un objeto {@link Sample}.
     *
     * @param string String de entrada.
     * @return Objeto {@link Sample} resultante.
     */
    Sample stringToSample(String string);

    /**
     * Transforma un Dataset de objetos String en otro de objetos {@link Sample}.
     *
     * @param stringDs Dataset de objetos String.
     * @return Dataset de objetos {@link Sample}.
     */
    Dataset<Sample> toSampleDs(Dataset<String> stringDs);

    /**
     * Devuelve un Dataset de objetos {@link DomainTypeCount} que resumen la cantidad de objetos
     * {@link Sample} del Dataset de entrada agrupados por el valor de su atributo
     * {@link Sample#domainType}
     *
     * @param samples
     * @return
     */
    Dataset<DomainTypeCount> getDomainTypeCounts(Dataset<Sample> samples);

    /**
     * Funcion equivalente a reduceByKey de la clase {@code RDD} que aplica una función de
     * reducción por clave un Dataset de objetos genéricos evitando el intercambio supérfluo de
     * datos entre las distintas particiones.
     *
     * @param input                    Dataset a reducir.
     * @param keyFunction              Funcion para extraer la clave de los objetos contenidos en {@code input}.
     * @param reducePartitionsFunction Función que agrupa registros dentro de cada partición del
     *                                 Dataset de entrada.
     * @param reduceGroupsFunction     Función que combina los resultados parciales de cada partición.
     * @param resultEncoder            Encoder que indica a Spark como serializar los objetos resultantes.
     * @param <T>                      Tipo de los objetos del Dataset entrante.
     * @param <U>                      Tipo de los objetos del Dataset resultante.
     * @return Dataset que combina por clave los objetos del Dataset entrante.
     */
    <T, U> Dataset<U> reduceByKey(
        Dataset<T> input,
        MapFunction<U, String> keyFunction,
        MapPartitionsFunction<T, U> reducePartitionsFunction,
        MapGroupsFunction<String, U, U> reduceGroupsFunction,
        Encoder<U> resultEncoder
    );
}
